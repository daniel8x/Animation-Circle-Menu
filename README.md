# Animation Circle Menu
Animation Circle Menu


## Challenge 

* Create	a	menu	that	begins	as	a	single	button.	
* When	clicked,	the	button	animates	and	creates	five buttons	equally	surrounding	it
* Each	button	surrounding	will	be	a	different	color.
* When	any	one	button is	clicked,	that	button’s	color	and	icon	animate	creating	a	circle	around	the	original	menu	button.
* The	button	option	then	animate	away	and	the	background	color	of	the	page	changes	to	the	color	of	the	
button	selected.

## Demo

![alt tag](http://corpuschl.com/demo.gif)

## Requirement

* HTML5 / CSS3
* Javascript : jQuery 1.11.2 library

## What's included
```
Animation-Circle-Menu
├── css/
│   ├── main.css
│   ├── reset.css
├── js/
│   ├── js.js
│   └── jquery-1.11.2.min.js
├── img/
│   ├── camera.png
│   ├── mouse.png
│   ├── phone.png
│   ├── printer.png
│   ├── setting.png
│   └── user.png
├── index.html

```
## Installation
Download project at :
[Click here to download] (https://github.com/daniel8x/Animation-Circle-Menu/archive/master.zip)

## Usage
* Click index.html to run project
* In order to change the distance from center button to surrounding button and the speed of running cirle around center button, 
just need to access to js.js file and change 2 variables below

```
var radius 			    = 150; 		// The distance from center button to surrounding button - px 
var speedCircle         = 1000;		// Set default speed running circle - miliseconds
```

## License

Free to use


