/*
*************************************************************************************************
 * JQuery file : js.js
 * Version:   1.0 
 * Date:      03/07/2016
 * Author:    Duc Trinh - Daniel
 * Email:     danieltrinh29@gmail.com
 * Website:   https://github.com/daniel8x/Animation-Circle-Menu
 *
**************************************************************************************************
*/
  
 
$(document).ready(function(){

 // Declare common variable

var radius 			= 150; 										// The distance from center button to surrounding button. 
var speedCircle     		= 800;									    // Set default speed running circle - miliseconds

var nButtons 		        = 5; 										// Number of surround-buttons
var _x 				= $('.center-button').position().left -50;  // Declare X : get X coordinate value of center button
var _y 				= $('.center-button').position().top  -50; 	// Declare Y: get Y coordinate value of center button
var sectors			= 360/nButtons; 							// unit sector
var rad 			= Math.PI / 180;							// convert from degrees to radians
var angle 			= -90;										// Initial angle
var new_x 			= 0.0, 
	new_y 			= 0.0;										// New X, Y coordinate
var counter 			= 0; 										// Declare Count variable
var id 				= 0;										// Declare surround button index variable
var arrAngles			= [5];                                      // Store angles of 5 surrounding button
var runCircle_new_x		= 0,
    runCircle_new_y		= 0;

// Auto generate 5 surround-button equally around center button

$( '.surround-button li' ).each(function( index ) {  

   new_x = _x + Math.cos(angle * rad) * radius;			// Generate new X coordinate 
   new_y = _y + Math.sin(angle * rad) * radius;			// Generate new Y coordinate
   
   $('.button-'+index).css({
	   'left':(new_x).toFixed(2) + 'px',
	   'top' :(new_y).toFixed(2) + 'px'
   });        
   arrAngles[index] = angle;
   angle += sectors;									// Caculate new angle for new surround button position
   
}); 


// Set event click to center button
$( '.center-button' ).on("click", function() {
     $('.surround-button').toggleClass('active');
});



// Set event click to surround button
$('.surround-button li').on("click", function() {
	id = $(this).index();										   
	angle = arrAngles[id];                                         // Get current angle of selected button
	sectors = 4; 												   // Set sectors unit;
	var bgColor = $( '.button-'+id ).css( "background-color" );    // Get background-color of selected button
    var img = $(this).html();
	 // Create animation of surround button run around center button
	var runCircle = function()
	 {
		 counter++;
		 angle += sectors;
		 runCircle_new_x 	= _x + Math.cos(angle * rad) * radius;  // Generate new X coordinate
		 runCircle_new_y    	= _y + Math.sin(angle * rad) * radius;  // Generate new Y coordinate
		 $('.surround-button ul').append(
						'<li class="button-' + id + 
						'" style="left:' + (runCircle_new_x).toFixed(2) + 'px;' +
						         'top:'  + (runCircle_new_y).toFixed(2) + 'px;">' 
								         +  img + '</li>');         // Generate list of Li to create circle animation
		
		// Check if running circle comes back to the original position
		if(counter >= (360/sectors)) {
			$('.button-'+id).not(':first').remove();			    // Remove running circle except the first one
			$('.surround-button').toggleClass('inactive');		
			$('body').css({											// Change bgColor 
				'backgroundColor'       :bgColor,
				'transition'            :'3s',
				'-webkit-transition'    :'3s',
				'-moz-transition'	:'3s',
				'-o-transition'         :'3s'
			}); 
			clearInterval(timer);									// Clear timer
		}
	 }
	 
	var timer = setInterval(runCircle, speedCircle/(360/sectors));				// Set timer to run circle							
	
});//-- End Event Click Surround Button

}); //-- End document Jquery
